from django import template
from datetime import datetime

register = template.Library()

def to_timestamp(time):
    return datetime.timestamp(time)

register.filter('timestamp', to_timestamp)