from celery import Celery
from celery.schedules import crontab
from .models import Event, Match, Team, Bet
from datetime import datetime, timedelta, date
import pytz
import tbapy

tba = tbapy.TBA('pSgdHRxLvPo0Keb1T8KVa3mrnbGxPk2ITS6oQcno6Q64Guj5EHxEGQ9NPUxtle9n')

celery = Celery('tasks', broker='pyamqp://localhost//')


@celery.task(name='get_events')
def addEvent():
    eventList = tba.events(2022)
    for event in eventList:
        if Event.objects.filter(name = event.name).exists() == False:
            if (datetime.strptime(event.end_date, '%Y-%m-%d') >= datetime.today() and datetime.strptime(event.start_date, '%Y-%m-%d') <= (datetime.today() + timedelta(days=6))) or datetime.strptime(event.end_date, '%Y-%m-%d') == datetime.today():
                key = event.key
                name = event.name
                city = event.city
                state = event.state_prov
                start_date = event.start_date
                end_date = event.end_date
                Event.objects.create(key=key, name=name, city=city, state=state, start_date=start_date, end_date=end_date)
                addMatches(key)
                addTeams(key)
        elif datetime.strptime(event.end_date, '%Y-%m-%d') + timedelta(days=1) < datetime.today():
            ended_event = Event.objects.get(name = event.name)
            ended_event.over = True
            ended_event.save()
        else:
            addMatches(event.key)
            addTeams(event.key)
    return 'events added'
    
    
def addMatches(event_key):
    matchList = tba.event_matches(event_key, True)
    for match in matchList:
        if Match.objects.filter(key = match.key).exists() == False:
            key = match.key
            if match.predicted_time is not None and match.predicted_time != 0:
                time = datetime.fromtimestamp(int(match.predicted_time))
                number = match.match_number
                red0 = match.alliances.get("red").get("team_keys")[0].split("frc",1)[1]
                red1 = match.alliances.get("red").get("team_keys")[1].split("frc",1)[1]
                red2 = match.alliances.get("red").get("team_keys")[2].split("frc",1)[1]
                blue0 = match.alliances.get("blue").get("team_keys")[0].split("frc",1)[1]
                blue1 = match.alliances.get("blue").get("team_keys")[1].split("frc",1)[1]
                blue2 = match.alliances.get("blue").get("team_keys")[2].split("frc",1)[1]
                teams = [red0, red1, red2, blue0, blue1, blue2]
                event = Event.objects.get(key = event_key)
                Match.objects.create(time=time, number=number, red0=red0, red1=red1, red2=red2, blue0=blue0, blue1=blue1, blue2=blue2, key=key, event=event)
    return 'matches added'

@celery.task(name='update_matches')
def updateMatches():
    matches = Match.objects.filter(played = False)
    for match in matches:
        predicted_red_score = 0
        predicted_blue_score = 0
        if hasattr(tba.event_oprs(match.event.key), 'oprs'):
            redOPR0 = tba.event_oprs(match.event.key).oprs.get("frc"+match.red0)
            redOPR1 = tba.event_oprs(match.event.key).oprs.get("frc"+match.red1)
            redOPR2 = tba.event_oprs(match.event.key).oprs.get("frc"+match.red2)
            if redOPR0 is not None and redOPR1 is not None and redOPR2 is not None:
                predicted_red_score = redOPR0 + redOPR1 + redOPR2
            blueOPR0 = tba.event_oprs(match.event.key).oprs.get("frc"+match.blue0)
            blueOPR1 = tba.event_oprs(match.event.key).oprs.get("frc"+match.blue1)
            blueOPR2 = tba.event_oprs(match.event.key).oprs.get("frc"+match.blue2)
            if blueOPR0 is not None and blueOPR1 is not None and blueOPR2 is not None:
                predicted_blue_score = blueOPR0 + blueOPR1 + blueOPR2
            if predicted_blue_score > predicted_red_score:
                match.predicted_winner = Match.BLUE
                match.predicted_margin = predicted_blue_score - predicted_red_score
            elif predicted_red_score > predicted_blue_score:
                match.predicted_winner = Match.RED
                match.predicted_margin = predicted_red_score -predicted_blue_score
            else:
                match.predicted_winner = Match.TIE
                match.predicted_margin = 0
        else:
            match.predicted_margin = 0
        tba_match = tba.match(match.key)
        if hasattr(tba_match, 'predicted_time') and tba_match.predicted_time is not None:
            match.time = datetime.fromtimestamp(int(tba_match.predicted_time))
        if hasattr(tba_match, 'score_breakdown') and match.played == False:
            if tba_match.score_breakdown is not None:
                match.actual_margin = abs(tba_match.score_breakdown.get("blue").get("totalPoints") - tba_match.score_breakdown.get("red").get("totalPoints"))
                if tba_match.score_breakdown.get("red").get("totalPoints") > tba_match.score_breakdown.get("blue").get("totalPoints"):
                    match.actual_winner = Match.RED
                elif tba_match.score_breakdown.get("blue").get("totalPoints") > tba_match.score_breakdown.get("red").get("totalPoints"):
                    match.actual_winner = Match.BLUE
                elif tba_match.score_breakdown.get("blue").get("totalPoints") == tba_match.score_breakdown.get("red").get("totalPoints"):
                    match.actual_winner = Match.TIE
                else:
                    updateMatches()
                # Set the state of the match to played so we don't keep doing these tasks for mathces we know are done.
                match.played = True
                # Give users the winnings from their bets
                match.apply_bets()
                # Update analytics for teams
                match.save()
                updateTeam(match.red0, match)
                updateTeam(match.red1, match)
                updateTeam(match.red2, match)
                updateTeam(match.blue0, match)
                updateTeam(match.blue1, match)
                updateTeam(match.blue2, match)
        if match.event.over:
            match.played = True
            match.actual_winner = Match.TIE
            match.apply_bets()
            
        match.save()
    return 'matches updated'

def addTeams(event_key):
    teams = tba.event_teams(event_key, True)
    if Event.objects.filter(key = event_key).exists():
        event = Event.objects.get(key = event_key)
        for team in teams:
            if Team.objects.filter(number = team.get("team_number")).exists() == False:
                number = team.get("team_number")
                name = team.get("name")
                nickname = team.get("nickname")
                city = team.get("city")
                state_prov = team.get("state_prov")
                country = team.get("country")
                newTeam = Team.objects.create(name = name, nickname = nickname, number=number, city = city, state_prov = state_prov, country = country, bets = 0, pointsWon = 0)
                event.teams.add(newTeam)
            else:
                event.teams.add(Team.objects.get(number = team.get("team_number")))

def updateTeam(team_number, match):
    if Team.objects.filter(number=team_number).exists():
        team = Team.objects.get(number=team_number)
        if int(match.red0) == team.number or int(match.red1) == team.number or int(match.red2) == team.number:
            if match.actual_winner == Match.RED:
                bets = Bet.objects.filter(match=match).filter(placed_on=Match.RED).all()
                otherBets = Bet.objects.filter(match=match).filter(placed_on=Match.BLUE).all()
                team.bets += len(bets) + len(otherBets)
                totalPoints = 0
                for bet in bets:
                    totalPoints += bet.winnings - bet.amount
                team.pointsWon += totalPoints
                print(team.number)
                team.save()
        elif int(match.blue0) == team.number or int(match.blue0) == team.number or int(match.blue2) == team.number:
            if match.actual_winner == Match.BLUE:
                bets = Bet.objects.filter(match=match).filter(placed_on=Match.BLUE).all()
                otherBets = Bet.objects.filter(match=match).filter(placed_on=Match.RED).all()
                team.bets += len(bets) + len(otherBets)
                totalPoints = 0
                for bet in bets:
                    totalPoints += bet.winnings - bet.amount
                team.pointsWon += totalPoints
                print(team.number)
                team.save()
    