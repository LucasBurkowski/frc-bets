from django import forms
from .models import Match

class BetForm(forms.Form):
    matchID = forms.ModelMultipleChoiceField(Match.objects)
    betOn = forms.ChoiceField(label='betOn', choices=(('R', 'Red'),('B', 'Blue')))
    betAmount = forms.FloatField(label='betAmount', min_value=0.0)


class MatchForm(forms.Form):
    matchID = forms.ModelMultipleChoiceField(Match.objects)