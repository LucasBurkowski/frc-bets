from django.contrib import admin
from .models import Event, Match, Bet, Team

# Register your models here.
admin.site.register(Event)
admin.site.register(Match)
admin.site.register(Bet)
admin.site.register(Team)