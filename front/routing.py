from django.conf.ursl import urls

from . import consumers

websocket_urlpatterns = [
    url(r'^ws/betting/(?P<match_name>[^/]+)/$', consumers.BetConsumer),
    ]