# Generated by Django 2.2.7 on 2019-12-03 18:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('front', '0003_auto_20191127_1922'),
    ]

    operations = [
        migrations.AlterField(
            model_name='match',
            name='actual_winner',
            field=models.CharField(blank=True, choices=[('R', 'Red alliance'), ('B', 'Blue alliance')], max_length=1),
        ),
        migrations.AlterField(
            model_name='match',
            name='predicted_winner',
            field=models.CharField(choices=[('R', 'Red alliance'), ('B', 'Blue alliance')], max_length=1),
        ),
    ]
