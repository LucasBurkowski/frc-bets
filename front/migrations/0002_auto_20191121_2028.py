# Generated by Django 2.2.7 on 2019-11-21 20:28

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('front', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='event',
            old_name='location',
            new_name='city',
        ),
        migrations.RemoveField(
            model_name='event',
            name='dates',
        ),
        migrations.AddField(
            model_name='event',
            name='end_date',
            field=models.CharField(default=django.utils.timezone.now, max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='event',
            name='key',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='event',
            name='start_date',
            field=models.CharField(default=django.utils.timezone.now, max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='event',
            name='state',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
        migrations.CreateModel(
            name='Match',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('time', models.DateTimeField()),
                ('number', models.IntegerField()),
                ('red0', models.CharField(max_length=100)),
                ('red1', models.CharField(max_length=100)),
                ('red2', models.CharField(max_length=100)),
                ('blue0', models.CharField(max_length=100)),
                ('blue1', models.CharField(max_length=100)),
                ('blue2', models.CharField(max_length=100)),
                ('predicted_winner', models.CharField(choices=[('R', 'Red alliance'), ('B', 'Blue alliance')], max_length=1)),
                ('predicted_margin', models.IntegerField(default=0)),
                ('actual_winner', models.CharField(blank=True, choices=[('R', 'Red alliance'), ('B', 'Blue alliance')], max_length=1)),
                ('key', models.CharField(max_length=100)),
                ('event', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='event_matches', to='front.Event')),
            ],
        ),
        migrations.CreateModel(
            name='Bet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.FloatField()),
                ('placed_on', models.CharField(choices=[('R', 'Red alliance'), ('B', 'Blue alliance')], max_length=1)),
                ('match', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='match_bets', to='front.Match')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_bets', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
