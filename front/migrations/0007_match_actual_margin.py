# Generated by Django 2.2.7 on 2019-12-05 00:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('front', '0006_bet_winnings'),
    ]

    operations = [
        migrations.AddField(
            model_name='match',
            name='actual_margin',
            field=models.IntegerField(default=0),
        ),
    ]
