from ws4redis.publisher import RedisPublisher
from ws4redis.redis_store import RedisMessage
from django.shortcuts import render, get_object_or_404
from .models import Event, Match, Bet, Team
from django.contrib.auth.models import User
from users.models import User, Record
from django.http import HttpResponse, HttpResponseServerError, HttpResponseBadRequest
from django.contrib.sessions.models import Session
from django.contrib.auth.decorators import login_required
from django.utils.timezone import make_naive
from django.conf import settings
from django.db.models import Count
from datetime import datetime, timedelta
from .bet_form import BetForm, MatchForm
import json


# Create your views here.
def home(request): 
    match = Match.objects.annotate(betCount=Count('match_bets')).order_by('-betCount').first();
    teams = Team.objects.all().order_by('-bets')[0:10]
    context = {
        'events': Event.objects.filter(over=False).order_by('start_date'),
        'match': match,
        'teams': teams
    }
    return render(request, 'front/home.html', context)
    
def insights(request):
    teams = Team.objects.all().order_by('-bets')[0:10]
    teamPoints = Team.objects.all().order_by('-pointsWon')[0:10]
    matches = Match.objects.annotate(betCount=Count('match_bets')).order_by('-betCount')[0:10];
    context = {
        'teams': teams,
        'teamPoints': teamPoints,
        'matches': matches
    }
    return render(request, 'front/insights.html', context)
    
def about(request):
    return render(request, 'front/about.html', {'title': 'About'})
    
def terms(request):
    return render(request, 'front/terms.html', {'title': 'Terms'})
    
def thanks(request):
    return render(request, 'front/thanks.html', {'title': 'Thanks'})

def event_detail(request, pk):
    #event = get_object_or_404(Event, pk=pk)
    event = get_object_or_404(Event.objects.filter(key=pk))
    matches = event.event_matches.order_by('time').all()
    teams = event.teams.order_by('number').all()
    teamBets = event.teams.order_by('-bets')[0:10]
    if request.user.is_authenticated:
        user_bets = Bet.objects.filter(owner=request.user)
        for match in matches:
            user_bet_on_match = user_bets.filter(match=match)
            if len(user_bet_on_match) == 1:
                match.add_user_bet(user_bet_on_match.get())
        
    context = {
        'event':event,
        'matches':matches,
        'teams' :teams,
        'teamBets' :teamBets
    }
    return render(request, 'front/event_detail.html', context)
    
def match_detail(request, mt):
    match = get_object_or_404(Match.objects.filter(key=mt))
    if request.user.is_authenticated:
        user_bet = Bet.objects.filter(owner=request.user).filter(match=match)
        if len(user_bet) > 0:
            match.add_user_bet(get_object_or_404(user_bet))
    
    context = {
        'match':match,
        'matches':[match],
    }
    return render(request, 'front/match_detail.html', context)
    
def team_detail(request, tm):
    team = get_object_or_404(Team.objects.filter(number=tm))
    
    context = {
        'team' :team,
    }
    return render(request, 'front/team_detail.html', context)

def leaderboard(request):
    records = users.records

@login_required
def my_bets(request):
    user_bets_active = Bet.objects.filter(owner=request.user).filter(active=True).order_by('match__time')
    matches_active = [bet.get_match_with_user_bet() for bet in user_bets_active.all()]
    
    user_bets_closed = Bet.objects.filter(owner=request.user).filter(active=False).order_by('match__time')
    matches_closed = [bet.get_match_with_user_bet() for bet in user_bets_closed.all()]
    
    events = []
    for match in matches_closed:
        if match.event not in events:
            events.append(match.event)
    
    context = {
        'matches_active':matches_active,
        'matches_closed':matches_closed,
        'events':events
    }
    
    return render(request, 'front/my_bets.html', context)
    
def my_bets_events(request, ev):
    user_bets_closed = Bet.objects.filter(owner=request.user).filter(active=False).order_by('match__time')
    event_bets = []
    
    for bet in user_bets_closed:
        if bet.match.event.name == ev and bet.match not in event_bets:
            match = bet.get_match_with_user_bet()
            event_bets.append(match)
    context = {
        'event_bets':event_bets,
        'event_name':ev
    }
    
    return render(request, 'front/my_bets_events.html', context)

@login_required
def PlaceBet(request):
    if request.method == 'POST':
        form = BetForm(request.POST)
        
        # Make sure that the form has all the necessary data in the proper formats
        if form.is_valid():
            cleaned_data = form.cleaned_data
            
            user = request.user
            
            match = cleaned_data['matchID'].get()
            
            # Make sure the user has enough points to make this bet
            if cleaned_data['betAmount'] < 0 or cleaned_data['betAmount'] > user.record.points:
                print("Insufficient points to place bet")
                return HttpResponseBadRequest("Insufficient points")
            
            # Make sure the bet is within the bet cap
            if cleaned_data['betAmount'] > settings.GLOBAL_SETTINGS['BET_CAP']:
                return HttpResponseBadRequest("Bet higher than bet cap")
            
            # Make sure that betting is still open for this match
            if make_naive(match.time) < datetime.utcnow():
                print("Match already happened!")
                return HttpResponseBadRequest("Betting closed")
            
            # Make sure the user has not yet bet on the match
            if Bet.objects.filter(owner=user).filter(match=match).count() > 0:
                print("User has already bet on match")
                return HttpResponseBadRequest("Already placed bet")
            
            Bet.objects.create(owner=request.user, match=match,
                amount=cleaned_data['betAmount'], placed_on=cleaned_data['betOn'])
            request.user.record.points -= cleaned_data['betAmount']
            request.user.record.save()
            
            facility = 'bets'
            audience = {'broadcast': True}
            
            message_data = {
                'matchID':match.id,
                'redBet':match.get_bets_on_red(),
                'blueBet':match.get_bets_on_blue()
            }
            
            redis_publisher = RedisPublisher(facility=facility, **audience)
            message = RedisMessage(json.dumps(message_data))
            redis_publisher.publish_message(message)
            return HttpResponse()
        else:
            print(form.errors)
            return HttpResponseBadRequest("Invalid bet entry")
    else:
        return HttpResponseBadRequest()


@login_required
def modifyBet(request):
    if request.method == 'POST':
        form = BetForm(request.POST)
        
        # Make sure that the form has all the necessary data in the proper formats
        if form.is_valid():
            cleaned_data = form.cleaned_data
            
            user = request.user
            
            match = cleaned_data['matchID'].get()
            
            bet = get_object_or_404(Bet.objects.filter(owner=user).filter(match=match))
            
            bet_amount_change = cleaned_data['betAmount']
            
            # Make sure the user has enough points to make this bet
            if cleaned_data['betAmount'] < 0 or bet_amount_change > user.record.points:
                print("Insufficient points to place bet")
                return HttpResponseBadRequest("Insufficient points")
            
            # Make sure the bet is within the bet cap
            if cleaned_data['betAmount'] > settings.GLOBAL_SETTINGS['BET_CAP']:
                return HttpResponseBadRequest("Bet higher than bet cap")
            
            # Make sure that betting is still open for this match
            if make_naive(match.time) < datetime.utcnow():
                print("Match already happened!")
                return HttpResponseBadRequest("Betting closed")
            
            bet.amount = cleaned_data['betAmount']
            bet.placed_on = cleaned_data['betOn'] 
            bet.save()
            
            request.user.record.points -= bet_amount_change
            request.user.record.save()
            
            facility = 'bets'
            audience = {'broadcast': True}
            
            message_data = {
                'matchID':match.id,
                'redBet':match.get_bets_on_red(),
                'blueBet':match.get_bets_on_blue()
            }
            
            redis_publisher = RedisPublisher(facility=facility, **audience)
            message = RedisMessage(json.dumps(message_data))
            redis_publisher.publish_message(message)
            return HttpResponse()
        else:
            print(form.errors)
            return HttpResponseBadRequest("Invalid bet entry")
    else:
        return HttpResponseBadRequest()


@login_required
def deleteBet(request):
    if request.method == 'POST':
        form = MatchForm(request.POST)
        
        # Make sure that the form has all the necessary data in the proper formats
        if form.is_valid():
            cleaned_data = form.cleaned_data
            
            user = request.user
            
            match = cleaned_data['matchID'].get()
            
            bet = get_object_or_404(Bet.objects.filter(owner=user).filter(match=match))
            
            bet.clear_bet()
            
            bet.delete()
            
            facility = 'bets'
            audience = {'broadcast': True}
            
            message_data = {
                'matchID':match.id,
                'redBet':match.get_bets_on_red(),
                'blueBet':match.get_bets_on_blue()
            }
            
            redis_publisher = RedisPublisher(facility=facility, **audience)
            message = RedisMessage(json.dumps(message_data))
            redis_publisher.publish_message(message)
            return HttpResponse()
        else:
            print(form.errors)
            return HttpResponseBadRequest("Invalid bet entry")
    else:
        return HttpResponseBadRequest()


def test(request):
    return render(request, 'front/test.html')
    
