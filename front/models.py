from django.db import models
from django.contrib.auth.models import User
from django.db.models import Sum
from datetime import datetime
from django.utils.timezone import make_naive
from django.conf import settings

class Team(models.Model):
    name = models.CharField(max_length=100)
    nickname = models.CharField(max_length=100)
    number = models.IntegerField()
    city = models.CharField(max_length=100)
    state_prov = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    bets = models.IntegerField()
    pointsWon = models.IntegerField()

# Event Model for FRC Event to Display
class Event(models.Model):
    name = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    start_date = models.CharField(max_length=100)
    end_date = models.CharField(max_length=100)
    key = models.CharField(max_length=100)
    over = models.BooleanField(default=False)
    teams = models.ManyToManyField(Team, related_name='event_teams')
    
class Match(models.Model):
    # Used for choices field
    RED = 'R'
    BLUE = 'B'
    TIE = 'T'
    winner_choices = [(RED, 'Red alliance'), (BLUE, 'Blue alliance'), (TIE, 'Tie')]
    time = models.DateTimeField()
    number = models.IntegerField()
    red0 = models.CharField(max_length=100)
    red1 = models.CharField(max_length=100)
    red2 = models.CharField(max_length=100)
    blue0 = models.CharField(max_length=100)
    blue1 = models.CharField(max_length=100)
    blue2 = models.CharField(max_length=100)
    predicted_winner = models.CharField(max_length=1, default=TIE, choices=winner_choices)
    predicted_margin = models.IntegerField(default=0)
    actual_winner = models.CharField(max_length=1, blank=True, choices=winner_choices)
    actual_margin = models.IntegerField(default=0)
    key = models.CharField(max_length=100)
    played = models.BooleanField(default=False)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, blank=True, null=True, related_name='event_matches')
    
    def get_bets_on(self, placed_on):
        amount = Bet.objects.filter(match=self, placed_on=placed_on).aggregate(Sum('amount'))['amount__sum']
        if amount == None:
            return 0
        else:
            return amount
    
    def get_bet_objects_on(self, placed_on):
        return Bet.objects.filter(match=self, placed_on=placed_on)
    
    def get_bets_on_blue(self):
        return self.get_bets_on(Bet.BLUE)
    
    def get_bet_objects_on_blue(self):
        return self.get_bet_objects_on(Bet.BLUE)

    def get_bets_on_red(self):
        return self.get_bets_on(Bet.RED)
        
    def get_bet_objects_on_red(self):
        return self.get_bet_objects_on(Bet.RED)
    
    def add_user_bet(self, user_bet):
        self.user_bet = user_bet
    
    def is_betting_open(self):
        return make_naive(self.time) > datetime.utcnow()
    
    # Update users' totals once a match winner is posted
    def apply_bets(self):
        for bet in self.match_bets.all():
            bet.apply_bet()
            
    def update_teams(self):
        Team.objects.get(number = red0).pointsWon += self.get_bet_objects_on_red
        Team.objects.get(number = red1).pointsWon += self.get_bet_objects_on_red
        Team.objects.get(number = red2).pointsWon += self.get_bet_objects_on_red
        Team.objects.get(number = blue0).pointsWon += self.get_bet_objects_on_blue
        Team.objects.get(number = blue1).pointsWon += self.get_bet_objects_on_blue
        Team.objects.get(number = blue2).pointsWon += self.get_bet_objects_on_blue
        if self.match.actual_winner == RED:
            Team.objects.get(number = red0).bets += 1
            Team.objects.get(number = red1).bets += 1
            Team.objects.get(number = red2).bets += 1
        elif self.match.actual_winner == BLUE:
            Team.objects.get(number = blue0).bets += 1
            Team.objects.get(number = blue1).bets += 1
            Team.objects.get(number = blue2).bets += 1
        
    # For when a match is deleted without being played.
    # Gives users back points bet on the match.
    def clear_bets(self):
        for bet in self.match_bets.all():
            bet.clear_bet()

class Bet(models.Model):
    # Used for choices field
    RED = 'R'
    BLUE = 'B'
    placed_on_choices = [(RED, 'Red alliance'), (BLUE, 'Blue alliance')]
    
    owner = models.ForeignKey(User, on_delete=models.CASCADE, blank=False, null=False, related_name='user_bets')
    match = models.ForeignKey(Match, on_delete=models.CASCADE, blank=False, null=False, related_name='match_bets')
    
    amount = models.FloatField()
    placed_on = models.CharField(max_length=1, choices=placed_on_choices)
    
    active = models.BooleanField(default=True)
    winnings = models.FloatField(default=0)
    
    def get_match_with_user_bet(self):
        match = self.match
        match.add_user_bet(self)
        return match
        
    def get_event(self):
        event = self.match.event
        return event
    
    # Give the user points for winning/losing
    def apply_bet(self):
        owner_record = self.owner.record
        self.active = False
        
        if self.match.actual_winner == self.placed_on: # If the user wins, give them points
            red_bets = self.match.get_bets_on_red()
            blue_bets = self.match.get_bets_on_blue()
            
            # Make machine bets.
            # I think this is enough if statements to be considered AI.
            if self.match.predicted_winner == Match.RED: # Red was predicted to win, bet more on red
                red_bets += settings.GLOBAL_SETTINGS['MACHINE_WINNER_BET']
                blue_bets += settings.GLOBAL_SETTINGS['MACHINE_BET'] - settings.GLOBAL_SETTINGS['MACHINE_WINNER_BET']
            elif self.match.predicted_winner == Match.BLUE: # Blue was predicted to win, bet more on blue
                blue_bets += settings.GLOBAL_SETTINGS['MACHINE_WINNER_BET']
                red_bets += settings.GLOBAL_SETTINGS['MACHINE_BET'] - settings.GLOBAL_SETTINGS['MACHINE_WINNER_BET']
            else: # Match was predicted tie, the machine bets equally on both teams
                red_bets += settings.GLOBAL_SETTINGS['MACHINE_BET'] / 2.0
                blue_bets += settings.GLOBAL_SETTINGS['MACHINE_BET'] / 2.0
            
            # Set the total bets of the winner and loser
            if self.match.actual_winner == Match.RED:
                winner_bet_total = red_bets
                loser_bet_total = blue_bets
            elif self.match.actual_winner == Match.BLUE:
                winner_bet_total = blue_bets
                loser_bet_total = red_bets
            
            proportion_of_winnings = self.amount / winner_bet_total
            self.winnings = proportion_of_winnings * (winner_bet_total + loser_bet_total)
            
            owner_record.points += self.winnings
            
            # Old winnings calculation system
            # if self.match.actual_winner == self.match.predicted_winner: # The user bet on the alliance already likely to win, give them some ponits, but not a lot
            #     self.winnings = (((self.match.actual_margin/self.match.predicted_margin) * self.amount) + self.amount * 1.5)
            # else: #Otherwise the user bet on the 'underdog' and should recieve a larger sum as reward
            #     self.winnings = ((((self.match.actual_margin/self.match.predicted_margin) * self.amount) * 2) + self.amount * 2)
            # owner_record.points = owner_record.points + self.winnings
            owner_record.bets_won = owner_record.bets_won + 1
        elif self.match.actual_winner == Match.TIE: # Check if the bet is a tie. If so, just give the points back
            owner_record.points = owner_record.points + self.amount
            self.winnings = self.amount
        else: # If the user loses, they get no points
            owner_record.bets_lost = owner_record.bets_lost + 1
            self.winnings = 0
        
        owner_record.save()
        self.save()
        
    # Give back points and set the bet to inactive.
    def clear_bet(self):
        owner_record = self.owner.record
        owner_record.points = owner_record.points + self.amount
        self.active = False
        
        owner_record.save()
        self.save()