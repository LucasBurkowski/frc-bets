
jQuery(document).ready(function($) {
var ws4redis = WS4Redis({
    uri: websocket_uri + 'test?subscribe-broadcast&publish-broadcast&echo',
    connecting: on_connecting,
    receive_message: receiveMessage,
    disconnected: on_disconnected
});

function on_connecting() {
    console.log('Websocket is connecting...');
}

function on_disconnected(evt) {
    console.log('Websocket was disconnected: ' + JSON.stringify(evt));
}

// receive a message though the websocket from the server
function receiveMessage(msg) {
    console.log('Message from Websocket: ' + msg);
}
});

var sendPost = function(){
    $.ajax({
        type: "POST",
        url: "/place-bet/",
        data: { csrfmiddlewaretoken: csrf_token,
                state:"inactive",
                betOn:$("input[name='alliance']:checked").val(),
                betAmount:$("input[id='amount']").val()
              },
        success: function() {
            console.log("Request successful")
        }
    })
};