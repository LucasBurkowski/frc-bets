var selectedMatch = -1;
var oldUserBet = -1;

function formatNumber(x) {
  return Number.parseFloat(x).toFixed(2);
}

jQuery(document).ready(function($) {
    
    var ws4redis = WS4Redis({
        uri: websocket_uri + 'bets?subscribe-broadcast&publish-broadcast',
        connecting: on_connecting,
        receive_message: receiveMessage,
        disconnected: on_disconnected
    });
    
    function on_connecting() {
        console.log('Websocket is connecting...');
    }
    
    function on_disconnected(evt) {
        console.log('Websocket was disconnected: ' + JSON.stringify(evt));
    }
    
    // receive a message though the websocket from the server
    function receiveMessage(msg) {
        
        console.log('Message from Websocket: ' + msg);
        var messageObj = JSON.parse(msg)
        
        var id = messageObj.matchID
        
        var rows = $("."+id)
        if(rows.length != 2){
            return
        }
        var row = rows[0]
        
        rows.find("#redBet")[0].innerHTML=formatNumber(messageObj.redBet) + ' 💸'
        rows.find('#blueBet')[0].innerHTML=formatNumber(messageObj.blueBet) + ' 💸'
    }
});

var sendPost = function(){
    var betOn = $("#betModal input[name='alliance']:checked").val()
    var betAmount = $("#betModal input[id='amount']").val()
    $.ajax({
        type: "POST",
        url: "/place-bet/",
        data: { csrfmiddlewaretoken: csrf_token,
                state:"inactive",
                matchID:selectedMatch,
                betOn:betOn,
                betAmount:betAmount
              },
        success: function() {
            console.log("Request successful")
            
            $('#betModal').modal('hide')
            
            var rows = $("."+selectedMatch)
            if(rows.length != 2){
                return
            }
            
            var user_bet = rows.find('#userBet')[0]
            
            var onRed = betOn === 'R'
            
            user_bet.innerHTML = '<button type="button" class="btn btn-light update-bet" data-toggle="modal" data-target="#modifyBetModal" onclick="setMatch(' + selectedMatch + ')">'
                        + formatNumber(betAmount) + ' 💸 on ' + (onRed ? '<b>red</b>' : '<b>blue</b>') + ' 💸</button>';
            user_bet.className = onRed ? 'red' : 'blue'
            
            var user_points = $('#user-points')[0]
            var points = parseFloat(user_points.innerText)
            user_points.innerText = formatNumber(points - betAmount) + ' 💸'
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
            alert("Error: " + XMLHttpRequest.responseText + ".\nBet could not be placed.");
            console.error("Error placing bet.\Response text: " + XMLHttpRequest.responseText +"\nError: " + errorThrown)
        }
    })
};

var sendPostModify = function(){
    var betOn = $("#modifyBetModal input[name='alliance']:checked").val()
    var betAmount = $("#modifyBetModal input[id='amount']").val()
    $.ajax({
        type: "POST",
        url: "/modify-bet/",
        data: { csrfmiddlewaretoken: csrf_token,
                state:"inactive",
                matchID:selectedMatch,
                betOn:betOn,
                betAmount:betAmount
              },
        success: function() {
            console.log("Request successful")
            
            $('#modifyBetModal').modal('hide')
            
            var rows = $("."+selectedMatch)
            if(rows.length != 2){
                return
            }
            
            var user_bet = rows.find('#userBet')[0]
            
            var onRed = betOn === 'R'
            
            user_bet.innerHTML = '<button type="button" class="btn btn-light update-bet" data-toggle="modal" data-target="#modifyBetModal" onclick="setMatch(' + selectedMatch + ')">'
                        + formatNumber(betAmount) + ' 💸 on ' + (onRed ? '<b>red</b>' : '<b>blue</b>') + '</button>';
            user_bet.className = onRed ? 'red' : 'blue'
            
            var user_points = $('#user-points')[0]
            var points = parseFloat(user_points.innerText)
            user_points.innerText = formatNumber(points - betAmount + oldUserBet) + ' 💸'
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
            alert("Error: " + XMLHttpRequest.responseText + ".\nBet could not be placed.");
            console.error("Error placing bet.\Response text: " + XMLHttpRequest.responseText +"\nError: " + errorThrown)
        }
    })
};

var sendPostDelete = function(){
    $.ajax({
        type: "POST",
        url: "/delete-bet/",
        data: { csrfmiddlewaretoken: csrf_token,
                state:"inactive",
                matchID:selectedMatch
              },
        success: function() {
            console.log("Request successful")
            
            var rows = $("."+selectedMatch)
            if(rows.length != 2){
                return
            }
            
            var user_bet = rows.find('#userBet')[0]
            
            user_bet.innerHTML = '<button type="button" class="btn btn-primary place-bet" data-toggle="modal" data-target="#betModal" onclick="setMatch('
                + selectedMatch + ')">Place bet</button>'
            user_bet.className = ''
            
            var user_points = $('#user-points')[0]
            var points = parseFloat(user_points.innerText)
            user_points.innerText = formatNumber(points + oldUserBet) + ' 💸'
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
            alert("Error: " + XMLHttpRequest.responseText + ".\nBet could not be placed.");
            console.error("Error placing bet.\Response text: " + XMLHttpRequest.responseText +"\nError: " + errorThrown)
        }
    })
};

var setMatch = function(matchID){
    selectedMatch=matchID;
    console.log("Selected match with ID " + matchID);
    
    var matchRowQuery = $("."+matchID)
    var matchRow = matchRowQuery[0];
    var matchString = matchRow.childNodes[1].innerText;
    
    $(".match-title")[0].innerHTML = matchString;
    $(".match-title")[1].innerHTML = matchString;
    
    var updateBtn = matchRowQuery.find('.update-bet')
    if(updateBtn.length == 1){
        oldUserBet = parseFloat(updateBtn[0].innerText)
    }
}