"""FRCBets URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.contrib.auth import views as auth_views
from front import views as front_views
from users import views as user_views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', front_views.home, name="home"),
    path('test/', front_views.test, name='test'),
    path('place-bet/', front_views.PlaceBet, name='place_bet'),
    path('modify-bet/', front_views.modifyBet, name='modify_bet'),
    path('delete-bet/', front_views.deleteBet, name='delete_bet'),
    path('event/<str:pk>/', front_views.event_detail, name='event_detail'),
    path('match/<str:mt>/', front_views.match_detail, name='match_detail'),
    path('users/<str:ui>/', user_views.publicProfile, name='publicProfile'),
    path('about/', front_views.about, name="about"),
    path('my-bets/', front_views.my_bets, name='my_bets'),
    path('my-bets/<str:ev>/', front_views.my_bets_events, name='my_bets_events'),
    path('terms/', front_views.terms, name='terms'),
    path('thanks/', front_views.thanks, name='thanks'),
    path('admin/', admin.site.urls),
    path('register/', user_views.register, name='register'),
    path('profile/', user_views.profile, name='profile'),
    path('leaderboard/', user_views.leaderboard, name='leaderboard'),
    path('insights/', front_views.insights, name='insights'),
    path('login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),
    path('oauth/', include('social_django.urls', namespace='social')),
    path(r'^admin/statuscheck/', include('celerybeat_status.urls')),
    path(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        user_views.activate, name='activate'),
    path('password-reset/',
         auth_views.PasswordResetView.as_view(
             template_name='users/password_reset.html'
         ),
         name='password_reset'),
    path('password-reset/done/',
         auth_views.PasswordResetDoneView.as_view(
             template_name='users/password_reset_done.html'
         ),
         name='password_reset_done'),
    path('password-reset-confirm/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(
             template_name='users/password_reset_confirm.html'
         ),
         name='password_reset_confirm'),
    path('password-reset-complete/',
         auth_views.PasswordResetCompleteView.as_view(
             template_name='users/password_reset_complete.html'
         ),
         name='password_reset_complete'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
