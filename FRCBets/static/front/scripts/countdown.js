jQuery(document).ready(function($) {
    
    function pad(number){
        return ("0" + number).slice (-2);
    }
    
    function getCurrentUnixTime(){
        return new Date().getTime() / 1000.0
    }
    
    function timeToGo(timestamp) {
        return timestamp - getCurrentUnixTime()
    }
    
    function toTimeString(diff){
        if(diff <= 0){
            return 'Betting closed'
        }

        // Get time components
        var days = Math.floor(diff/86400)
        var hours = diff%86400 / 3600| 0;
        var mins  = diff%3600 / 60 | 0;
        var secs  = Math.round(diff%60);
    
        // Return formatted string
        return (days > 0 ? (days == 1? '1 day ': days + ' days ') : '') + (hours > 0 ? pad(hours) + ':' : '') + pad(mins) + ':' + pad(secs);   
    }
    
    function getTimestamp(timer, index){
        return parseFloat(timer.textContent)
    }
    
    function updateTimer(timer, endTimestamp){
        var time = timeToGo(endTimestamp)
        
        // This is inefficient to do every second for every match, but it's not
        // nearly bad enough to be problematic yet, so I'm not optimizing it.
        
        // Remove the bet button if betting is closed
        if(time <= 0){
            // Get the <td> containing the bet button
            var userBet = $(timer.parentNode).find('#userBet')
            
            // Look for the update bet button
            var updateButton = userBet.find('.update-bet')
            
            // If the update bet button is there, remove the button but leave the comment
            if(updateButton.length > 0){
                userBet[0].innerHTML = updateButton[0].innerHTML
            }
            
            // Look for the place bet button
            var placeButton =  userBet.find('.place-bet')
            
            // If the button is there, remove it
            if(placeButton.length > 0){
                userBet[0].removeChild(placeButton[0])
            }
        }
        
        timer.textContent = toTimeString(time)
    }
    
    var timers = jQuery.makeArray($(".time-left"))
    var endTimestamps = timers.map(getTimestamp)
    
    for(var i = 0; i < timers.length; i++){
        updateTimer(timers[i], endTimestamps[i])
    }
    
    window.setInterval(function(){
        for(var i = 0; i < timers.length; i++){
            updateTimer(timers[i], endTimestamps[i])
        }
    }, 1000);
});
