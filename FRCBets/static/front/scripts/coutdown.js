var timestamp = (Date.now() + 1000 * 60 * 60 * 24 * 3) - Date.now();

timestamp /= 1000; // from ms to seconds

function component(x, v) {
    return Math.floor(x / v);
}

var $div = $('div');

setInterval(function() {
    
    timestamp--;
    
    var days    = component(timestamp, 24 * 60 * 60),
        hours   = component(timestamp,      60 * 60) % 24,
        minutes = component(timestamp,           60) % 60,
        seconds = component(timestamp,            1) % 60;
    
    $div.html(days + " days, " + hours + ":" + minutes + ":" + seconds);
    
}, 1000);