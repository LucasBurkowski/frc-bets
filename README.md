# FRC-Bets

## Mission Statement
Making robot analysis more fun through competition.

## What is FRC-Bets?
FRC-Bets is a platform that allows you to bet on robot performance with and against others. Users compete for points that are awarded on sucsessful bets regarding match outcome. Bets are placed only on teams that the user believes will win, and on a correct prediction points are awarded. The catch is that if a user bets on an alliance or team with a large margin of predicted sucsess, the user will be awareded a proportionally smaller amount of points relative to another user who bets sucsessfully on a less probable outcome. 