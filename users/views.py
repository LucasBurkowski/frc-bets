from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .models import Record
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.http import HttpResponse
from front.models import Bet


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            mail_subject = 'Activate your FRCBets Account.'
            message = render_to_string('acc_active_email.html', {
		'user': user,
    		'domain': 'www.frcbets.com',
		'uid':urlsafe_base64_encode(force_bytes(user.pk)),
                'token':account_activation_token.make_token(user),
	    })
            to_email = form.cleaned_data.get('email')
            username = form.cleaned_data.get('username')
            email = EmailMessage(
			mail_subject, message, '"FRCBets.com" <admin@frcbets.com>', to=[to_email]
	    )
            email.send()
            return HttpResponse('Please confirm your email address to complete registration');
    else:
        form = UserRegisterForm()
    return render(request, 'users/register.html', {'form': form})

def leaderboard(request):
    records = Record.objects.order_by('-points')[0:10]
    
    if request.user.is_authenticated:
        p_record = Record.objects.get(user=request.user)
        context = { 
            'records': records,
            'p_record': p_record,
        }
    else:    
        context = {
            'records': records,
        }
    return render(request, 'users/leaderboard.html', context)

@login_required
def profile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST,
                                   request.FILES,
                                   instance=request.user.profile)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, 'Your account has been updated!')
            return redirect('profile')

    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'u_form': u_form,
        'p_form': p_form
    }

    return render(request, 'users/profile.html', context)
    
def publicProfile(request, ui):
    curr_user = get_object_or_404(User.objects.filter(username=ui))
    record = Record.objects.get(user=curr_user)
    user_bets_active = Bet.objects.filter(owner=curr_user).filter(active=True).order_by('match__time')
    matches_active = [bet.get_match_with_user_bet() for bet in user_bets_active.all()]
    
    user_bets_closed = Bet.objects.filter(owner=curr_user).filter(active=False).order_by('match__time')
    matches_closed = [bet.get_match_with_user_bet() for bet in user_bets_closed.all()]
    
    context = {
        'curr_user': curr_user,
        'record': record,
        'matches_active':matches_active,
        'matches_closed':matches_closed
    }
    
    return render(request, 'users/publicProfile.html', context)

def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        return redirect('login')
        #return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
    else:
        return HttpResponse('Activation link is invalid!')
