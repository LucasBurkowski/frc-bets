from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from PIL import Image


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.png', upload_to='profile_pics')
    team = models.CharField(max_length=4, validators=[RegexValidator(r'^\d{1,10}$')], null=True, blank=True)
    
    def __str__(self): 
        return self.user.username+ ' Profile'

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)
            
class Record(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bets_won = models.IntegerField(default=0)
    bets_lost = models.IntegerField(default=0)
    points = models.IntegerField(default=1000)
    
    def total_bets(self):
        return self.bets_won + self.bets_lost
    
    def __str__(self):
        return self.user.username+ ' Record'
    